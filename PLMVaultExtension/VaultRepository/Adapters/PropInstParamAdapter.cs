﻿using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Autodesk.Connectivity.WebServices;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    class PropInstParamAdapter : IPropInstParamAdapter
    {
        PropInstParam _adaptee;
        public PropInstParam Adaptee { get => _adaptee; }


        public PropInstParamAdapter(PropInstParam propInstParam)
        {
            _adaptee = propInstParam;
        }

        public long PropDefId
        {
            get
            {
                return Adaptee.PropDefId;
            }
        }

        public object Val
        {
            get
            {
                return Adaptee.Val;
            }
        }

    }
}
