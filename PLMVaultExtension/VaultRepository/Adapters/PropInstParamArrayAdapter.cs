﻿using Autodesk.Connectivity.WebServices;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    class PropInstParamArrayAdapter : IPropInstParamArrayAdapter
    {
        public PropInstParamArray _adaptee { get; }

        public PropInstParamArrayAdapter(PropInstParamArray propInstParamArray)
        {
            _adaptee = propInstParamArray;
        }

        public IPropInstParamAdapter[] propInstParam {
            get {
                    return _adaptee.Items.Select(item=> {
                        return new PropInstParamAdapter(item);
                    }).ToArray();
                }
        }

        
    }
}
