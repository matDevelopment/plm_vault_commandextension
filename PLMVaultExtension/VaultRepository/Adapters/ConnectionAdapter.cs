﻿using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using PLMVaultExtensionCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    class ConnectionAdapter : IDisposable, IConnectionAdapter
    {
        public Connection Adaptee { get; private set; }

        public ConnectionAdapter(Connection adaptee)
        {
            Adaptee = adaptee;
        }


        public IPropertyServiceAdapter PropertyService
        {
            get
            {
                return new PropertyServiceAdapter(Adaptee.WebServiceManager.PropertyService);
            }
        }

        public IItemServiceAdapter ItemService
        {
            get
            {
                return new ItemServiceAdapter(Adaptee.WebServiceManager.ItemService);
            }
        }

        public void Dispose()
        {
            if (Adaptee != null && Adaptee.WebServiceManager != null)
            {
                Adaptee.WebServiceManager.Dispose();
            }
        }
    }
}
