﻿using Autodesk.Connectivity.WebServices;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    class PropDefAdapter : IPropDefAdapter
    {
        PropDef _adaptee;

        public PropDefAdapter(PropDef prop)
        {
            this._adaptee = prop;
        }

        public string DispName
        {
        get {
                return _adaptee.DispName;
         }
        }

        public long Id
        {
            get
            {
                return _adaptee.Id;
            }
        }
    }
}
