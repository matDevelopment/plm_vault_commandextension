﻿using Autodesk.Connectivity.WebServices;
using PLMVaultExtensionCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System.Linq;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    class PropertyServiceAdapter : IDisposable,IPropertyServiceAdapter
    {
        private PropertyService _adaptee;

        public PropertyServiceAdapter(PropertyService propertyService)
        {
            this._adaptee = propertyService;
        }

        public void Dispose()
        {
            if (_adaptee != null)
            {
                _adaptee.Dispose();
            }
        }

        IPropDefAdapter[] IPropertyServiceAdapter.GetPropertyDefinitionsByEntityClassId(string entityClassId)
        {
          return _adaptee.GetPropertyDefinitionsByEntityClassId("ITEM").Select(prop =>
          {
              return new PropDefAdapter(prop);
          }).ToArray();
        }
    }
}
