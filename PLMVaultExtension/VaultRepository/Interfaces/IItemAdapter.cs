﻿using Autodesk.Connectivity.WebServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Interfaces
{
    public interface IItemAdapter
    {
        Item _adaptee { get; }
        long RevId { get; }
    }
}
