﻿using Autodesk.Connectivity.WebServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Interfaces
{
    public interface IPropInstParamArrayAdapter
    {
        PropInstParamArray _adaptee {get;}
        IPropInstParamAdapter[] propInstParam { get; }
    }
}
