﻿using PLMVaultExtensionCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Interfaces
{
   public interface ILogInResultAdapter
    {
        IConnectionAdapter Connection { get; }
        Dictionary<string, string> ErrorMessages { get; }
        bool Success { get; }
    }
}
