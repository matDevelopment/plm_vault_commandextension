﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.Interfaces
{
    public interface IConnectionAdapter : IDisposable
    {
        IPropertyServiceAdapter PropertyService { get; }
        IItemServiceAdapter ItemService { get; }
    }
}
