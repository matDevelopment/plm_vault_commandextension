﻿using Autodesk.Connectivity.WebServices;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.Interfaces
{
   public interface IPropertyServiceAdapter
    {
        IPropDefAdapter[] GetPropertyDefinitionsByEntityClassId(string entityClassId);
    }
}
