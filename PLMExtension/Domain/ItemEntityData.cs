﻿using PLMVaultExtensionCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMExtension.Domain
{
    class ItemEntityData : IEntityData
    {
        public string Type { get; set; } = "ITEM";
        public long Id { get; set; }
        public Dictionary<string, string> Properties { get; set; } = new Dictionary<string, string>();
    }
}
