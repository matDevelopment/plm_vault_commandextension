﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMExtension.Domain
{
    public class TechnologyRoute
    {
        public string Name { get; set; }
        public string OperationSummary { get; set; }
    }
}
