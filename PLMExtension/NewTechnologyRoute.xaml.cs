﻿using PLMExtension.Domain;
using PLMExtension.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PLMExtension
{
    /// <summary>
    /// Interaction logic for NewTechnologyRoute.xaml
    /// </summary>
    public partial class NewTechnologyRoute : Window
    {
        private ITechnologyRoutesContainer technologyVM;

        public NewTechnologyRoute(ITechnologyRoutesContainer technologyVM)
        {
            this.technologyVM = technologyVM;
            InitializeComponent();
            metalSheetTechnologyRoute_Tab.ViewModel.Header = "Wybierz operację - Blacha";
            SetMetalSheetOperations();

            partSheetTechnologyRoute_Tab.ViewModel.Header = "Wybierz operację - Bryła";

            profileSheetTechnologyRoute_Tab.ViewModel.Header = "Wybierz operację - Profil";
        }

        private void SetMetalSheetOperations()
        {
            foreach (var operation in GlobalSettings.AllOperations)
            {
                metalSheetTechnologyRoute_Tab.ViewModel.Operations.Add(new OperationViewModel()
                {
                    Selected = false,
                    Operation = new Operation()
                    {
                        Abreviation = operation.Key,
                        Name = operation.Value
                    }
                });
            };
        }


        private void Cancel_BTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OK_BTN_Click(object sender, RoutedEventArgs e)
        {
            var operationSummary = metalSheetTechnologyRoute_Tab.ViewModel.OperationsSummary;
            var name = metalSheetTechnologyRoute_Tab.ViewModel.Name;

            if (!String.IsNullOrEmpty(operationSummary) && !technologyVM.TechnologyRoutes.Any(elem => String.Equals(elem.Name, operationSummary)))
            {
                technologyVM.TechnologyRoutes.Add(new TechnologyRouteViewModel()
                {
                    Name = name,
                    OperationSummary = operationSummary,
                    Selected = false
                });
                GlobalSettings.AvailableRoutes.Add(new TechnologyRoute()
                {
                    Name = name,
                    OperationSummary = operationSummary
                });
            }
            this.Close();
        }
    }
}

