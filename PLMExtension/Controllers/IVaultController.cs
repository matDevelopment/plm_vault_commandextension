﻿namespace PLMExtension.VaultImplementations
{
    public interface IVaultController
    {
        void SetSingleProperty(string propertyName, string propertyValue);
    }
}