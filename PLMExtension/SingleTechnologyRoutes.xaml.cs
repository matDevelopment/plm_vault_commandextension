﻿using PLMExtension.ViewModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PLMExtension
{
    /// <summary>
    /// Interaction logic for SingleTechnologyRoutes.xaml
    /// </summary>
    public partial class SingleTechnologyRoutes : UserControl
    {
        private SingleTechnologyViewModel viewModel = new SingleTechnologyViewModel();
        public SingleTechnologyViewModel ViewModel { get => viewModel; set => viewModel = value; }
        public SingleTechnologyRoutes()
        {
            InitializeComponent();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Operations != null && ViewModel.Operations.Any(x => x.Selected))
            {
                viewModel.OperationsSummary = ViewModel.Operations.Where(x => x.Selected).Select(y => y.Operation.Abreviation).Aggregate((current, next) => $"{current}.{next}");
            }
            else
            {
                viewModel.OperationsSummary = "";
            }
        }
    }
}
